# -*- coding: utf-8 -*-
'''
@author: Michał Grosicki
'''
import time
import cPickle

import numpy as np
import PIL.Image
from theano.tensor.shared_randomstreams import RandomStreams

from models.rbm import RBM
from util.datasets import load_data
from util.visualization import tile_raster_images


def rbm_mnist():
    print "Loading data..."
    datasets = load_data('src/data/mnist.pkl.gz')
    print "Data loaded, building model..."
    rng = np.random.RandomState(time.gmtime())
    theano_rng = RandomStreams(rng.randint(2 ** 30))
    n_hidden = 500
    n_visible = 28 * 28
    rbm = RBM(n_visible=n_visible, n_hidden=n_hidden, numpy_rng=rng,
              theano_rng=theano_rng)
    print "Training model..."
    n_chains = 20
    rbm.train(learning_rate=0.01, l1_reg=0, l2_reg=0, n_epochs=1,
              dataset=datasets[0][0], batch_size=20, n_chains=n_chains, k=1)
    f = file('/home/grosik/util/rbm.mdl', 'wb')
    cPickle.dump(rbm, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()
    f2 = file('/home/grosik/util/rbm.mdl', 'rb')
    rbm2 = cPickle.load(f2)
    f2.close()
    filters_image = tile_raster_images(
            X=rbm2.W.get_value(borrow=True).T,
            img_shape=(28, 28), tile_shape=(10, 10),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(filters_image)
    image.save('/home/grosik/util/rbm_mnist_filters.png')
    print "Training complete, sampling from the RBM..."
    samples = rbm2.sample(plot_every=1000, n_chains=n_chains,
                         n_samples=10, chain_init_state=datasets[2][0])
    samples_images = np.zeros((29 * 10 + 1, 29 * 20 - 1), dtype='uint8')
    for s in xrange(len(samples)):
        samples_images[29 * s: 29 * s + 28, :] = tile_raster_images(
            X=samples[s],
            img_shape=(28, 28),
            tile_shape=(1, n_chains),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(samples_images)
    image.save('/home/grosik/util/rbm_mnist_samples.png')

if __name__ == '__main__':
    rbm_mnist()
