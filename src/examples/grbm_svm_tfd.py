# -*- coding: utf-8 -*-
'''
@author: Michał Grosicki
'''
import time
import os
import errno
import cPickle
import csv

import numpy as np
import PIL.Image
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
import theano
from svm import svm_parameter, svm_problem
from svmutil import svm_train, svm_predict, svm_save_model
#from theano.sandbox.cuda.rng_curand import CURAND_RandomStreams as RandomStreams

from models.gaussian_rbm import GaussianRBM
from util.datasets import load_tfd, normalize, permute
from util.visualization import tile_raster_images


def grbm_svm_tfd():
    model_name = 'grbm_svm'
    data_name = 'tfd'
    data_path = '/home/grosik/util/' + '_'.join([model_name, data_name]) + '/'
    model_ext = '.mdl'
    image_ext = '.png'
    try:
        os.makedirs(data_path)
        print "Creating data directory: " + data_path
    except OSError as ex:
        if ex.errno != errno.EEXIST:
            raise
    print "Loading data..."
    data = load_tfd('/home/grosik/train.csv')
    data = permute(data[0], data[1])
    train_x = data[0]
    train_y = data[1]
#    test_x = data[0][2500:]
#    test_y = data[1][2500:]
    (test_x, test_y) = load_tfd('/home/grosik/test.csv')
    (train_x, mu, std) = normalize(train_x)
#    (valid_x, mu, std) = normalize(valid_x, mu, std)
    (test_x, mu, std) = normalize(test_x, mu, std)
#    datasets = ((train_x, train_y), (valid_x, valid_y), (test_x, test_y))
    print "Data loaded, building model..."
    rng = np.random.RandomState(time.gmtime())
    theano_rng = RandomStreams(rng.randint(2 ** 30))
    n_hidden = 500
    n_visible = 48 * 48
    rbm = GaussianRBM(n_visible=n_visible, n_hidden=n_hidden, numpy_rng=rng,
              theano_rng=theano_rng)
    print "Training model..."
    n_chains = 20
    learning_rate = 0.001
    l1_reg = 0.00
    l2_reg = 0.001
    n_epochs = 500
    k = 1
    batch_size = 20
    rbm.train(learning_rate=learning_rate, l1_reg=l1_reg, l2_reg=l2_reg,
              n_epochs=n_epochs, dataset=train_x, batch_size=batch_size,
              n_chains=n_chains, k=k, momentum=0.5)
    timestamp = str(time.time())
    name = str.join('_', [model_name, data_name, timestamp])
    f = file(data_path + name + model_ext, 'wb')
    cPickle.dump(rbm, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()
    print "Training complete."
    print "GRBM model saved as: " + name + model_ext + " in " + data_path
    filters_image = tile_raster_images(
            X=rbm.W.get_value(borrow=True).T,
            img_shape=(48, 48), tile_shape=(20, 20),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(filters_image)
    name = str.join('_', [model_name, data_name, 'filters', timestamp])
    image.save(data_path + name + image_ext)
    print "Visualisation of GRBM filters saved as: " + name + image_ext
    print "Sampling from the RBM..."
    n_samples = 10
    samples = rbm.sample(plot_every=1000, n_chains=n_chains,
                         n_samples=n_samples, chain_init_state=None)
    samples_images = np.zeros((49 * n_samples + 1, 49 * n_chains - 1),
                              dtype='uint8')
    for s in xrange(len(samples)):
        samples_images[49 * s: 49 * s + 48, :] = tile_raster_images(
            X=samples[s],
            img_shape=(48, 48),
            tile_shape=(1, n_chains),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(samples_images)
    name = str.join('_', [model_name, data_name, 'samples', timestamp])
    image.save(data_path + name + image_ext)
    print "Visualisation of GRBM samples saved as: " + name + image_ext
    print "Computing hidden representation of the data"
    data_fn = theano.function(inputs=[rbm.input],
                                      outputs=rbm.output,
                                      allow_input_downcast=True)
    train_x = data_fn(train_x)
    test_x = data_fn(test_x)
    print "Training SVM..."
    train_x = train_x.tolist()
    train_y = train_y.tolist()
    test_x = test_x.tolist()
    test_y = [0.0 for i in xrange(len(test_x))]
    problem = svm_problem(train_y, train_x)
    param = svm_parameter("-q")
#    param.svm_type = 1
    param.probability = 0
    param.kernel_type = 2
    param.cross_validation = 1
    param.nr_fold = 10
    param.eps = 0.0001
    svm_train(problem, param)
    param.cross_validation = 0
    model = svm_train(problem, param)
    timestamp = str(time.time())
    name = str.join('_', [model_name, data_name, timestamp])
    svm_save_model(name + model_ext, model)
    prediction = svm_predict(test_y, test_x, model)
    print prediction
    # convert 1D list of predictions to 2D list, this makes savig csv easier
    pred = [[int(p)] for p in prediction[0]]
    csv_file = open('/home/grosik/svm_pred.csv', 'wb')
    csv_writer = csv.writer(csv_file)
    # writer.writerows expects list of lists
    csv_writer.writerows(pred)
    csv_file.close()

if __name__ == "__main__":
    grbm_svm_tfd()
