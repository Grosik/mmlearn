# -*- coding: utf-8 -*-
'''
Copyright (c) 2008–2013, Theano Development Team All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
Neither the name of Theano nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ‘’AS IS’’ AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author: Michał Grosicki
'''
import time
import gzip
import cPickle
import errno
import os

import numpy as np
import PIL.Image
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

from util.datasets import load_tfd, normalize, permute
from util.visualization import tile_raster_images
from models.dbn import DBN
from models.mlp import MLP


def dbn_mlp_tfd():
    model_name = 'dbn_mlp'
    data_name = 'tfd'
    data_path = '/home/grosik/util/' + '_'.join([model_name, data_name]) + '/'
    model_ext = '.mdl'
    image_ext = '.png'
    try:
        os.makedirs(data_path)
        print "Creating data directory: " + data_path
    except OSError as ex:
        if ex.errno != errno.EEXIST:
            raise
    print "Loading data..."
    data = load_tfd('/home/grosik/train.csv')
    data = permute(data[0], data[1])
    train_x = data[0][0:2500]
    train_y = data[1][0:2500]
    valid_x = data[0][2500:]
    valid_y = data[1][2500:]
#    test_x = data[0][2500:]
#    test_y = data[1][2500:]
    (test_x, test_y) = load_tfd('/home/grosik/test.csv')
    (train_x, mu, std) = normalize(train_x)
    (valid_x, mu, std) = normalize(valid_x, mu, std)
    (test_x, mu, std) = normalize(test_x, mu, std)
    datasets = ((train_x, train_y), (valid_x, valid_y), (test_x, test_y))
    print "Data loaded, building model..."
    rng = np.random.RandomState(time.gmtime())
    theano_rng = RandomStreams(rng.randint(2 ** 30))
    n_hidden = [500, 500]
    n_visible = 48 * 48

    dbn = DBN(n_visible=n_visible, hidden_layers_sizes=n_hidden, numpy_rng=rng,
              theano_rng=theano_rng)
    print "Training model..."
    n_chains = 10
    n_epochs = 500
    dbn.train(learning_rates=[0.01], l1_regs=[0],
              l2_regs=[0.001, 0.0], n_epochs=n_epochs, dataset=train_x,
              batch_sizes=[10], n_chains=n_chains, ks=[1], momentums=[0.5])
    timestamp = str(time.time())
    name = str.join('_', [model_name, data_name, timestamp])
    f = file(data_path + name + model_ext, 'wb')
    cPickle.dump(dbn, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()
    print "Training complete"
    print "DBN model saved as: " + name + model_ext + " in " + data_path

    filters_image = tile_raster_images(
            X=dbn.hidden_layers[0].W.get_value(borrow=True).T,
            img_shape=(48, 48), tile_shape=(50, 10),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(filters_image)
    name = str.join('_', [model_name, data_name, 'filters', timestamp])
    image.save(data_path + name + image_ext)
    print "Visualisation of first layer filters saved as: " + name + image_ext
    print "Sampling from the DBN..."
    samples = dbn.sample(plot_every=1000, n_chains=n_chains, n_samples=10)
    samples_images = np.zeros((49 * 10 + 1, 49 * n_chains - 1), dtype='uint8')
    for s in xrange(len(samples)):
        samples_images[49 * s: 49 * s + 48, :] = tile_raster_images(
            X=samples[s],
            img_shape=(48, 48),
            tile_shape=(1, n_chains),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(samples_images)
    name = str.join('_', [model_name, data_name, 'samples', timestamp])
    image.save(data_path + name + image_ext)
    print "Visualisation of DBN samples saved as: " + name + image_ext

    print "Training MLP..."
    mlp = MLP(n_visible, [500, 500, 250], 7, params=dbn.params(), rng=rng)
    mlp.train(0.001, 0.0001, 0.000, 5000, datasets, 10, 50000, momentum=0.5)

    timestamp = str(time.time())
    name = str.join('_', [model_name, data_name, timestamp])
    f = file(data_path + name + model_ext, 'wb')
    cPickle.dump(mlp, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()
    print "Training complete."
    print "MLP model saved as: " + name + model_ext + " in " + data_path

    filters_image = tile_raster_images(
            X=mlp.hidden_layers[0].W.get_value(borrow=True).T,
            img_shape=(48, 48), tile_shape=(20, 20),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(filters_image)
    name = str.join('_', [model_name, data_name, 'filters', timestamp])
    image.save(data_path + name + image_ext)
    print "Visualisation of MLP filters saved as: " + name + image_ext

if __name__ == '__main__':
    dbn_mlp_tfd()
