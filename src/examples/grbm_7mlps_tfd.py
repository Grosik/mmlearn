# -*- coding: utf-8 -*-
'''
@author: Michał Grosicki
'''
import time
import os
import errno
import cPickle

import numpy as np
import PIL.Image
import theano
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
#from theano.sandbox.cuda.rng_curand import CURAND_RandomStreams as RandomStreams

from models.mlp import MLP
from models.gaussian_rbm import GaussianRBM
from util.datasets import load_tfd, normalize, permute, group_by_class
from util.visualization import tile_raster_images


def grbm_7mlps_tfd():
    model_name = 'grbm_7mlps'
    data_name = 'tfd'
    data_path = '/home/grosik/util/' + '_'.join([model_name, data_name]) + '/'
    model_ext = '.mdl'
    image_ext = '.png'
    try:
        os.makedirs(data_path)
        print "Creating data directory: " + data_path
    except OSError as ex:
        if ex.errno != errno.EEXIST:
            raise
    print "Loading data..."
    data = load_tfd('/home/grosik/train.csv')
    data = permute(data[0], data[1])
    train_x = data[0][0:2500]
    train_y = data[1][0:2500]
    valid_x = data[0][2500:]
    valid_y = data[1][2500:]
#    test_x = data[0][2500:]
#    test_y = data[1][2500:]
    (test_x, test_y) = load_tfd('/home/grosik/test.csv')
    (train_x, mu, std) = normalize(train_x)
    (valid_x, mu, std) = normalize(valid_x, mu, std)
    (test_x, mu, std) = normalize(test_x, mu, std)
    datasets = ((train_x, train_y), (valid_x, valid_y), (test_x, test_y))
#    faces_image = tile_raster_images(
#            X=train_x,
#            img_shape=(48, 48), tile_shape=(10, 10),
#            tile_spacing=(1, 1))
#    image = PIL.Image.fromarray(faces_image)
#    image.save('/home/grosik/util/tfd.png')
    print "Data loaded, building model..."
    rng = np.random.RandomState(time.gmtime())
    theano_rng = RandomStreams(rng.randint(2 ** 30))
    n_hidden = 100
    n_visible = 48 * 48
#    rbm = GaussianRBM(n_visible=n_visible, n_hidden=n_hidden, numpy_rng=rng,
#              theano_rng=theano_rng)
    print "Training model..."
    n_chains = 20
    learning_rate = 0.001
    l1_reg = 0.00
    l2_reg = 0.001
    n_epochs = 300
    k = 1
    batch_size = 20
#    rbm.train(learning_rate=learning_rate, l1_reg=l1_reg, l2_reg=l2_reg,
#              n_epochs=n_epochs, dataset=train_x, batch_size=batch_size,
#              n_chains=n_chains, k=k, momentum=0.5)
#    timestamp = str(time.time())
#    name = str.join('_', [model_name, data_name, timestamp])
#    f = file(data_path + name + model_ext, 'wb')
#    cPickle.dump(rbm, f, protocol=cPickle.HIGHEST_PROTOCOL)
#    f.close()
#    print "Training complete."
#    print "GRBM model saved as: " + name + model_ext + " in " + data_path
#    filters_image = tile_raster_images(
#            X=rbm.W.get_value(borrow=True).T,
#            img_shape=(48, 48), tile_shape=(10, 10),
#            tile_spacing=(1, 1))
#    image = PIL.Image.fromarray(filters_image)
#    name = str.join('_', [model_name, data_name, 'filters', timestamp])
#    image.save(data_path + name + image_ext)
#    print "Visualisation of GRBM filters saved as: " + name + image_ext
#    print "Sampling from the RBM..."
#    n_samples = 10
#    samples = rbm.sample(plot_every=1000, n_chains=n_chains,
#                         n_samples=n_samples, chain_init_state=None)
#    samples_images = np.zeros((49 * n_samples + 1, 49 * n_chains - 1),
#                              dtype='uint8')
#    for s in xrange(len(samples)):
#        samples_images[49 * s: 49 * s + 48, :] = tile_raster_images(
#            X=samples[s],
#            img_shape=(48, 48),
#            tile_shape=(1, n_chains),
#            tile_spacing=(1, 1))
#    image = PIL.Image.fromarray(samples_images)
#    name = str.join('_', [model_name, data_name, 'samples', timestamp])
#    image.save(data_path + name + image_ext)
#    print "Visualisation of GRBM samples saved as: " + name + image_ext
    print "Data loaded, building model..."
    grouped_train = group_by_class(train_x, train_y)
    grouped_valid = group_by_class(valid_x, valid_y)
    for i in xrange(7):
        # extract "positive" training examples (ith emotion)
        pos_train_x = [grouped_train[k] for k in grouped_train if k == i]
        # extract "negative" training examples
        neg_train_x = [grouped_train[k] for k in grouped_train if k != i]
        # flatten list
        neg_train_x = [e for examples in neg_train_x for e in examples]
        # repeat for validation set
        pos_valid_x = [grouped_valid[k] for k in grouped_valid if k == i]
        neg_valid_x = [grouped_valid[k] for k in grouped_valid if k != i]
        neg_valid_x = [e for examples in neg_valid_x for e in examples]
        # generate labels
        pos_train_y = [1 for j in xrange(len(pos_train_x[0]))]
        neg_train_y = [0 for j in xrange(len(neg_train_x[0]))]
        pos_valid_y = [1 for j in xrange(len(pos_valid_x[0]))]
        neg_valid_y = [0 for j in xrange(len(neg_valid_x[0]))]
        this_train_x = np.concatenate((pos_train_x[0], neg_train_x[:len(pos_train_x[0])]))
        this_train_y = np.concatenate((pos_train_y, neg_train_y))
        this_valid_x = np.concatenate((pos_valid_x[0], neg_valid_x[:len(pos_valid_x[0])]))
        this_valid_y = np.concatenate((pos_valid_y, neg_valid_y))
        train_pair = permute(this_train_x, this_train_y)
        those_datasets = (train_pair, (this_valid_x, this_valid_y),
                          (test_x, test_y))
        rbm = GaussianRBM(n_visible=n_visible, n_hidden=n_hidden, numpy_rng=rng,
              theano_rng=theano_rng)
        rbm.train(learning_rate=learning_rate, l1_reg=l1_reg, l2_reg=l2_reg,
              n_epochs=n_epochs, dataset=this_train_x, batch_size=batch_size,
              n_chains=n_chains, k=1, momentum=0.5)
        mlp = MLP(n_visible, [100], 2, params=[rbm.params], rng=rng)
        print "Training MLP number: " + str(i + 1) + "..."
        mlp.train(0.01, 0.00, 0.001, 500, those_datasets, 20, 100000,
                  momentum=0.5)
        if not i:
            W = mlp.hidden_layers[0].W.get_value(borrow=True)
            b = mlp.hidden_layers[0].b.get_value(borrow=True)
        else:
            W = np.concatenate((W,
                                mlp.hidden_layers[0].W.get_value(borrow=True)),
                               axis=1)
            b = np.concatenate((b,
                                mlp.hidden_layers[0].b.get_value(borrow=True)),
                               axis=0)
    params = [theano.shared(value=W, name='W', borrow=True),
              theano.shared(value=b, name='b', borrow=True)]
    mlp = MLP(n_visible, [700], 7, params=[params], rng=rng)
    print "Training final MLP"
    mlp.train(0.001, 0.00, 0.001, 2000, datasets, 20, 100000, momentum=0.5)
    timestamp = str(time.time())
    name = str.join('_', [model_name, data_name, timestamp])
    f = file(data_path + name + model_ext, 'wb')
    cPickle.dump(mlp, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()
    print "Training complete."
    print "MLP model saved as: " + name + model_ext + " in " + data_path
    filters_image = tile_raster_images(
            X=mlp.hidden_layers[0].W.get_value(borrow=True).T,
            img_shape=(48, 48), tile_shape=(20, 20),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(filters_image)
    name = str.join('_', [model_name, data_name, 'filters', timestamp])
    image.save(data_path + name + image_ext)
    print "Visualisation of MLP filters saved as: " + name + image_ext

if __name__ == '__main__':
    grbm_7mlps_tfd()
