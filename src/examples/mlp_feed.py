# -*- coding: utf-8 -*-
'''
@author: Michał Grosicki
'''
import time
import os
import errno
import cPickle

import numpy as np
import PIL.Image

from models.mlp import MLP
from models.layer import SigmoidLayer
from util.datasets import load_data
from util.visualization import tile_raster_images


def mlp_feed():
    model_name = 'mlp'
    data_name = 'feed'
    data_path = '/home/grosik/util/' + '_'.join([model_name, data_name]) + '/'
    model_ext = '.mdl'
    image_ext = '.png'
    try:
        os.makedirs(data_path)
        print "Creating data directory: " + data_path
    except OSError as ex:
        if ex.errno != errno.EEXIST:
            raise
    print "Loading data..."
    datasets = load_data(path='src/data/feed7.pkl.gz', normalize_data=True)
    rng = np.random.RandomState(time.gmtime())
    print "Data loaded, building model..."
    mlp = MLP(2500, [1024], 7, hidden_layers_type=SigmoidLayer, rng=rng)
    print "Training model..."
    mlp.train(0.001, 0.00005, 0.0000, 2000, datasets, 1, 100000, momentum=0.5)
    timestamp = str(time.time())
    name = str.join('_', [model_name, data_name, timestamp])
    f = file(data_path + name + model_ext, 'wb')
    cPickle.dump(mlp, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()
    print "Training complete."
    print "MLP model saved as: " + name + model_ext + " in " + data_path
    filters_image = tile_raster_images(
            X=mlp.hidden_layers[0].W.get_value(borrow=True).T,
            img_shape=(50, 50), tile_shape=(10, 10),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(filters_image)
    image.save(data_path + name + image_ext)
    print "Visualisation of MLP filters saved as: " + name + image_ext

if __name__ == '__main__':
    mlp_feed()
