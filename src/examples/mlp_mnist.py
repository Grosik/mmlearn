# -*- coding: utf-8 -*-
'''
@author: Michał Grosicki
'''
import time
import cPickle

import numpy as np
import PIL.Image

from models.mlp import MLP
from util.datasets import load_data
from util.visualization import tile_raster_images


def mlp_mnist():
    datasets = load_data('src/data/mnist.pkl.gz')
    rng = np.random.RandomState(time.gmtime())
    mlp = MLP(28 * 28, [500], 10, rng=rng)
    mlp.train(0.01, 0.00, 0.001, 100, datasets, 100, 50000, momentum=0.5)
    filters_image = tile_raster_images(
            X=mlp.hidden_layers[0].W.get_value(borrow=True).T,
            img_shape=(28, 28), tile_shape=(10, 10),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(filters_image)
    image.save('/home/grosik/util/mlp_mnist_filters2.png')
    f = file('/home/grosik/util/mlp.mdl', 'wb')
    cPickle.dump(mlp, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()
    f2 = file('/home/grosik/util/mlp.mdl', 'rb')
    mlp2 = cPickle.load(f2)
    filters_image = tile_raster_images(
            X=mlp2.hidden_layers[0].W.get_value(borrow=True).T,
            img_shape=(28, 28), tile_shape=(10, 10),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(filters_image)
    image.save('/home/grosik/util/mlp_mnist_filters.png')

if __name__ == '__main__':
    mlp_mnist()
