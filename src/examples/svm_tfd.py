# -*- coding: utf-8 -*-
'''
@author: Michał Grosicki
'''
import csv

from svm import svm_parameter, svm_problem
from svmutil import svm_train, svm_predict

from util.datasets import load_tfd, permute, normalize


def svm_tfd():
    print "Loading data..."
    data = load_tfd('/home/grosik/train.csv')
    data = permute(data[0], data[1])
    train_x = data[0]
    train_y = data[1]
    (test_x, test_y) = load_tfd('/home/grosik/test.csv')
    (train_x, mu, std) = normalize(train_x)
    (test_x, mu, std) = normalize(test_x, mu, std)
    train_x = train_x.tolist()
    train_y = train_y.tolist()
    test_x = test_x.tolist()
    test_y = [0.0 for i in xrange(len(test_x))]
    problem = svm_problem(train_y, train_x)
    param = svm_parameter("-q")
#    param.svm_type = 1
    param.probability = 0
    param.kernel_type = 2
    param.cross_validation = 1
    param.nr_fold = 10
    param.eps = 0.0001
    svm_train(problem, param)
    param.cross_validation = 0
    model = svm_train(problem, param)
    prediction = svm_predict(test_y, test_x, model)
    print prediction
    # convert 1D list of predictions to 2D list, this makes savig csv easier
    pred = [[int(p)] for p in prediction[0]]
    csv_file = open('/home/grosik/svm_pred.csv', 'wb')
    csv_writer = csv.writer(csv_file)
    # writer.writerows expects list of lists
    csv_writer.writerows(pred)
    csv_file.close()

if __name__ == "__main__":
    svm_tfd()
