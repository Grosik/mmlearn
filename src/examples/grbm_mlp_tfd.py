# -*- coding: utf-8 -*-
'''
@author: Michał Grosicki
'''
import os
import errno
import time
import cPickle

import numpy as np
import PIL.Image
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
#from theano.sandbox.cuda.rng_curand import CURAND_RandomStreams as RandomStreams

from models.mlp import MLP
from models.gaussian_rbm import GaussianRBM
from util.datasets import load_tfd, normalize, permute
from util.visualization import tile_raster_images


def grbm_mlp_tfd():
    model_name = 'grbm_mlp'
    data_name = 'tfd'
    data_path = '/home/grosik/util/' + '_'.join([model_name, data_name]) + '/'
    model_ext = '.mdl'
    image_ext = '.png'
    try:
        os.makedirs(data_path)
        print "Creating data directory: " + data_path
    except OSError as ex:
        if ex.errno != errno.EEXIST:
            raise
    print "Loading data..."
    data = load_tfd('/home/grosik/train.csv')
    data = permute(data[0], data[1])
    train_x = data[0][0:2500]
    train_y = data[1][0:2500]
    valid_x = data[0][2500:]
    valid_y = data[1][2500:]
#    test_x = data[0][2500:]
#    test_y = data[1][2500:]
    (test_x, test_y) = load_tfd('/home/grosik/test.csv')
    (train_x, mu, std) = normalize(train_x)
    (valid_x, mu, std) = normalize(valid_x, mu, std)
    (test_x, mu, std) = normalize(test_x, mu, std)
    datasets = ((train_x, train_y), (valid_x, valid_y), (test_x, test_y))
#    faces_image = tile_raster_images(
#            X=test_x,
#            img_shape=(48, 48), tile_shape=(30, 30),
#            tile_spacing=(1, 1))
#    image = PIL.Image.fromarray(faces_image)
#    image.save('/home/grosik/util/tfd.png')
    print "Data loaded, building model..."
    rng = np.random.RandomState(time.gmtime())
    theano_rng = RandomStreams(rng.randint(2 ** 30))
    n_hidden = 1024
    n_visible = 48 * 48
    rbm = GaussianRBM(n_visible=n_visible, n_hidden=n_hidden, numpy_rng=rng,
              theano_rng=theano_rng)
    print "Training model..."
    n_chains = 20
    learning_rate = 0.001
    l1_reg = 0.00001
    l2_reg = 0.0001
    n_epochs = 1000
    k = 1
    batch_size = 20
    rbm.train(learning_rate=learning_rate, l1_reg=l1_reg, l2_reg=l2_reg,
              n_epochs=n_epochs, dataset=train_x, batch_size=batch_size,
              n_chains=n_chains, k=k, momentum=0.5)
    timestamp = str(time.time())
    name = str.join('_', [model_name, data_name, timestamp])
    f = file(data_path + name + model_ext, 'wb')
    cPickle.dump(rbm, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()
    print "Training complete."
    print "GRBM model saved as: " + name + model_ext + " in " + data_path
    filters_image = tile_raster_images(
            X=rbm.W.get_value(borrow=True).T,
            img_shape=(48, 48), tile_shape=(20, 20),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(filters_image)
    name = str.join('_', [model_name, data_name, 'filters', timestamp])
    image.save(data_path + name + image_ext)
    print "Visualisation of GRBM filters saved as: " + name + image_ext
    print "Sampling from the RBM..."
    n_samples = 10
    samples = rbm.sample(plot_every=1000, n_chains=n_chains,
                         n_samples=n_samples, chain_init_state=None)
    samples_images = np.zeros((49 * n_samples + 1, 49 * n_chains - 1),
                              dtype='uint8')
    for s in xrange(len(samples)):
        samples_images[49 * s: 49 * s + 48, :] = tile_raster_images(
            X=samples[s],
            img_shape=(48, 48),
            tile_shape=(1, n_chains),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(samples_images)
    name = str.join('_', [model_name, data_name, 'samples', timestamp])
    image.save(data_path + name + image_ext)
    print "Visualisation of GRBM samples saved as: " + name + image_ext
    print "Data loaded, building model..."
    mlp = MLP(n_visible, [1024], 7, params=[rbm.params], rng=rng)
    print "Training model..."
    mlp.train(0.001, 0.00001, 0.001, 5000, datasets, 20, 100000, momentum=0.5)
    timestamp = str(time.time())
    name = str.join('_', [model_name, data_name, timestamp])
    f = file(data_path + name + model_ext, 'wb')
    cPickle.dump(mlp, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()
    print "Training complete."
    print "MLP model saved as: " + name + model_ext + " in " + data_path
    filters_image = tile_raster_images(
            X=mlp.hidden_layers[0].W.get_value(borrow=True).T,
            img_shape=(48, 48), tile_shape=(20, 20),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(filters_image)
    name = str.join('_', [model_name, data_name, 'filters', timestamp])
    image.save(data_path + name + image_ext)
    print "Visualisation of MLP filters saved as: " + name + image_ext

if __name__ == '__main__':
    grbm_mlp_tfd()
