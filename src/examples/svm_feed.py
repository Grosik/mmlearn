# -*- coding: utf-8 -*-
'''
@author: Michał Grosicki
'''

from svm import svm_parameter, svm_problem
from svmutil import svm_train, svm_predict
import numpy as np

from util.datasets import load_data, permute


def svm_feed():
    print "Loading data..."
    datasets = load_data(path='src/data/feed7.pkl.gz', normalize_data=True)
    train_x = np.concatenate((datasets[0][0], datasets[1][0]), axis=0)
    train_y = np.concatenate((datasets[0][1], datasets[1][1]), axis=0)
    (train_x, train_y) = permute(train_x, train_y)
    train_x = train_x.tolist()
    train_y = train_y.tolist()
    test_x = datasets[2][0].tolist()
    test_y = datasets[2][1].tolist()
    problem = svm_problem(train_y, train_x)
    param = svm_parameter("-q")
    param.svm_type = 1
    param.probability = 1
    param.kernel_type = 0
    param.cross_validation = 1
    param.eps = 0.0001
    param.nr_fold = 10
#    param.C = 100
    model = svm_train(problem, param)
    param.cross_validation = 0
    model = svm_train(problem, param)
    prediction = svm_predict(test_y, test_x, model)
    print prediction

if __name__ == "__main__":
    svm_feed()
