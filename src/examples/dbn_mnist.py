# -*- coding: utf-8 -*-
'''
Copyright (c) 2008–2013, Theano Development Team All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
Neither the name of Theano nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ‘’AS IS’’ AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author: Michał Grosicki
'''
import time
import cPickle

import numpy as np
import PIL.Image
from theano.tensor.shared_randomstreams import RandomStreams

from util.datasets import load_data
from util.visualization import tile_raster_images
from models.dbn import DBN


def dbn_mnist():
    print "Loading data..."
    datasets = load_data('src/data/mnist.pkl.gz')
    print "Data loaded, building model..."
    rng = np.random.RandomState(time.gmtime())
    theano_rng = RandomStreams(rng.randint(2 ** 30))
    n_hidden = [500, 500]
    n_visible = 28 * 28
    dbn = DBN(n_visible=n_visible, hidden_layers_sizes=n_hidden, numpy_rng=rng,
              theano_rng=theano_rng)
    print "Training model..."
    n_chains = 20
    dbn.train(learning_rates=[0.01], l1_regs=[0], l2_regs=[0], n_epochs=1,
              dataset=datasets[0][0], batch_sizes=[20], n_chains=n_chains, ks=[5])
#    f = file('/home/grosik/util/dbn.mdl', 'wb')
#    cPickle.dump(dbn, f, protocol=cPickle.HIGHEST_PROTOCOL)
#    f.close()
#    f2 = file('/home/grosik/util/dbn.mdl', 'rb')
#    dbn2 = cPickle.load(f2)
    print "Training complete, sampling from the DBN..."
    samples = dbn.sample(plot_every=1000, n_chains=n_chains, n_samples=10)
    samples_images = np.zeros((29 * 10 + 1, 29 * 20 - 1), dtype='uint8')
    for s in xrange(len(samples)):
        samples_images[29 * s: 29 * s + 28, :] = tile_raster_images(
            X=samples[s],
            img_shape=(28, 28),
            tile_shape=(1, n_chains),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(samples_images)
    image.save('/home/grosik/util/dbn_mnist_samples.png')
#    filters_image = tile_raster_images(
#            X=rbm.W.get_value(borrow=True).T,
#            img_shape=(28, 28), tile_shape=(10, 10),
#            tile_spacing=(1, 1))
#    image = PIL.Image.fromarray(filters_image)
#    image.save('/home/grosik/util/dbn_mnist_filters.png')


if __name__ == '__main__':
    dbn_mnist()
