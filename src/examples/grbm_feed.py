# -*- coding: utf-8 -*-
'''
@author: Michał Grosicki
'''
import time
import gzip
import cPickle
import os
import errno

import numpy as np
import PIL.Image
import theano
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

from models.gaussian_rbm import GaussianRBM
from util.datasets import normalize, permute, load_data, extract_patches
from util.visualization import tile_raster_images


def grbm_feed():
    model_name = 'grbm'
    data_name = 'feed'
    data_path = '/home/grosik/util/' + '_'.join([model_name, data_name]) + '/'
    model_ext = '.mdl'
    image_ext = '.png'
    try:
        os.makedirs(data_path)
        print "Creating data directory: " + data_path
    except OSError as ex:
        if ex.errno != errno.EEXIST:
            raise
    print "Loading data..."
#    dataset = load_data('src/data/faces.pkl.gz', normalize=True)
#    f = gzip.open('src/data/faces.pkl.gz', 'rb')
#    train_set = cPickle.load(f)
#    f.close()
    datasets = load_data(path='src/data/feed7.pkl.gz', normalize_data=False)
    train_set = extract_patches(images=datasets[0][0], image_size=(50, 50),
                                patch_size=(25, 25), stride=25)
#    train_set = np.concatenate((train_set, datasets[0][0]), axis=0)
    (train_set, std, mu) = normalize(train_set)
    train_set = permute(train_set)[0]
#    train_set = theano.shared(np.asarray(train_set[0], dtype=theano.config.floatX),
#                              borrow=True)
    print "Data loaded, building model..."
    rng = np.random.RandomState(time.gmtime())
    theano_rng = RandomStreams(rng.randint(2 ** 30))
    input_h = 25
    input_w = 25
    n_hidden = 1000
    n_visible = input_h * input_w
    rbm = GaussianRBM(n_visible=n_visible, n_hidden=n_hidden, numpy_rng=rng,
              theano_rng=theano_rng)
    print "Training model..."
    n_chains = 20
    learning_rate = 0.01
    l1_reg = 0.001
    l2_reg = 0.001
    n_epochs = 1000
    k = 1
    batch_size = 20
    rbm.train(learning_rate=learning_rate, l1_reg=l1_reg, l2_reg=l2_reg,
              n_epochs=n_epochs, dataset=train_set, batch_size=batch_size,
              n_chains=n_chains, k=k)
    timestamp = str(time.time())
    name = str.join('_', [model_name, data_name, timestamp])
    f = file(data_path + name + model_ext, 'wb')
    cPickle.dump(rbm, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()
    print "Training complete."
    print "GRBM model saved as: " + name + model_ext + " in " + data_path
#    sigma2_image = PIL.Image.fromarray(tile_raster_images(
#            X=np.asarray([rbm.sigma2.get_value(borrow=True)]),
#            img_shape=(50, 50), tile_shape=(1, 1),
#            tile_spacing=(1, 1)))
#    sigma2_image.save('/home/grosik/util/grbm_feed_sigma2.png')
    filters_image = tile_raster_images(
            X=rbm.W.get_value(borrow=True).T,
            img_shape=(input_h, input_w), tile_shape=(100, 10),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(filters_image)
    name = str.join('_', [model_name, data_name, 'filters', timestamp])
    image.save(data_path + name + image_ext)
    print "Visualisation of GRBM filters saved as: " + name + image_ext
#    f = file('/home/grosik/util/grbm_feed/grbm_feed_1363913747.2.mdl')
#    rbm = cPickle.load(f)
#    f.close()
    print "Sampling from the RBM..."
    n_samples = 10
    samples = rbm.sample(plot_every=1000, n_chains=n_chains,
                         n_samples=n_samples, chain_init_state=None)
    samples_images = np.zeros(((input_h + 1) * n_samples + 1, (input_w + 1) * n_chains - 1),
                              dtype='uint8')
    for s in xrange(len(samples)):
        samples_images[(input_w + 1) * s: (input_h + 1) * s + input_w, :] = tile_raster_images(
            X=samples[s],
            img_shape=(input_h, input_w),
            tile_shape=(1, n_chains),
            tile_spacing=(1, 1))
    image = PIL.Image.fromarray(samples_images)
    name = str.join('_', [model_name, data_name, 'samples', timestamp])
    image.save(data_path + name + image_ext)
    print "Visualisation of GRBM samples saved as: " + name + image_ext

if __name__ == '__main__':
    grbm_feed()
