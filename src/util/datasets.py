# -*- coding: utf-8 -*-
'''
@author: Michał Grosicki
'''

import cPickle
import gzip
import os
import csv

import theano
import theano.tensor as T
import numpy as np
import cv  # TODO change cv to PIL


def normalize(data, mean=None, std=None):
    if mean is None or std is None:
        mean = data.mean(axis=0)
        std = data.std(axis=0)

    norm_data = (data - mean) / std
    return (norm_data, mean, std)


def permute(data_x, data_y=None):
    perm = range(data_x.shape[0])
    np.random.shuffle(perm)
    data_x = data_x[perm]
    if data_y is not None:
        data_y = data_y[perm]
    return (data_x, data_y)


def load_feed(path, n_examples=(230, 84, 86), n_classes=7, extension='.jpg',
              size=(50, 50)):
    class_mapping = {'anger': 0, 'disgs': 1, 'fears': 2, 'happy': 3,
                     'sadns': 4, 'surpr': 5, 'neutr': 6}
    training_subjects = ['0001', '0002', '0003', '0004', '0005', '0006',
                         '0007', '0008', '0009', '0010', '1002']
    validation_subjects = ['0011', '0012', '0013', '0014']
    test_subjects = ['0015', '0016', '0017', '0018', '0019']
    subject_partition = (training_subjects, validation_subjects, test_subjects)    
    train_images = np.empty((n_examples[0], size[0] * size[1]))
    train_labels = np.zeros(n_examples[0])
    validation_images = np.empty((n_examples[1], size[0] * size[1]))
    validation_labels = np.zeros(n_examples[1])
    test_images = np.empty((n_examples[2], size[0] * size[1]))
    test_labels = np.zeros(n_examples[2])
    training_subjects = subject_partition[0]
    validation_subjects = subject_partition[1]
    test_subjects = subject_partition[2]
    train_count = 0
    validation_count = 0
    test_count = 0
    for root, subfolders, files in os.walk(path):    # traverse directory
        if subfolders:
            class_name = root[len(path):]
        for image_file in files:
            if str(image_file).endswith(extension):    # load every image with specified extension
                original = cv.LoadImage(str(root) + "/" + str(image_file))
                grayscale = cv.CreateImage(cv.GetSize(original), 8, 1)
                cv.CvtColor(original, grayscale, cv.CV_BGR2GRAY)
                image = cv.CreateImage(size, grayscale.depth, grayscale.nChannels)
                cv.Resize(grayscale, image)
                subject = root[-6:-2]
                if subject in training_subjects:
                    train_images[train_count, :] = np.array(image[:, :]).flatten()
                    train_labels[train_count] = class_mapping[class_name]
                    train_count += 1
                elif subject in validation_subjects:
                    validation_images[validation_count, :] = np.array(image[:, :]).flatten()
                    validation_labels[validation_count] = class_mapping[class_name]
                    validation_count += 1
                else:
                    test_images[test_count, :] = np.array(image[:, :]).flatten()
                    test_labels[test_count] = class_mapping[class_name]
                    test_count += 1
    return ((train_images, train_labels), (validation_images, validation_labels),
            (test_images, test_labels))


#images = load_faces(...)
#faces_file = gzip.GzipFile('../data/faces.pkl.gz', 'wb')
#faces_file.write(cPickle.dumps(images, protocol=cPickle.HIGHEST_PROTOCOL))
#faces_file.close()
def load_faces(path, extension='.jpg', size=(50, 50)):
    number_of_examples = 22783
    train_images = np.empty((number_of_examples, size[0] * size[1]))
    train_count = 0
    for root, subfolders, files in os.walk(path):
        for image_file in files:
            if str(image_file).endswith(extension):    # load every image with specified extension
                original = cv.LoadImage(str(root) + "/" + str(image_file))
                grayscale = cv.CreateImage(cv.GetSize(original), 8, 1)
                cv.CvtColor(original, grayscale, cv.CV_BGR2GRAY)
                image = cv.CreateImage(size, grayscale.depth, grayscale.nChannels)
                cv.Resize(grayscale, image)
                train_images[train_count, :] = np.array(image[:, :]).flatten()
                train_count += 1
    return train_images


def shared_dataset(data_xy, borrow=True):
    """ Function that loads the datasetinto shared variables
    The reason we store our dataset in shared variables is to allow
    Theano to copy it into the GPU memory (when code is run on GPU).
    Since copying data into the GPU is slow, copying a minibatch everytime
    is needed (the default behaviour if the data is not in a shared
    variable) would lead to a large decrease in performance.
    """
    data_x, data_y = data_xy
    shared_x = theano.shared(np.asarray(data_x, dtype=theano.config.floatX),
                             borrow=borrow)
    if data_y is None:
        shared_y = None
    else:
        shared_y = theano.shared(np.asarray(data_y, dtype=theano.config.floatX),
                             borrow=borrow)
        shared_y = T.cast(shared_y, 'int32')
    # When storing data on the GPU it has to be stored as floats
    # therefore we will store the labels as ``floatX`` as well
    # (``shared_y`` does exactly that). But during our computations
    # we need them as ints (we use labels as index, and if they are
    # floats it doesn't make sense) therefore instead of returning
    # ``shared_y`` we will have to cast it to int. This little hack
    # lets ous get around this issue
    return shared_x, shared_y


def load_data(path, normalize_data=False, mean=None, std=None):
    """Loads the path

    :type path: string
    :param path: the path to the path (here MNIST)

    """
    # Load the path
    f = gzip.open(path, 'rb')
    train_set, valid_set, test_set = cPickle.load(f)
    f.close()
    #train_set, valid_set, test_set format: tuple(input, target)
    #input is an numpy.ndarray of 2 dimensions (a matrix)
    #witch row's correspond to an example. target is a
    #numpy.ndarray of 1 dimensions (vector)) that have the same length as
    #the number of rows in the input. It should give the target
    #target to the example with the same index in the input.
    if normalize_data:
        if mean is not None and std is not None:
            (train_data, mean, std) = normalize(train_set[0], mean, std)
        else:
            (train_data, mean, std) = normalize(train_set[0])
        (valid_data, mean, std) = normalize(valid_set[0], mean, std)
        (test_data, mean, std) = normalize(test_set[0], mean, std)
        train_set = (train_data, train_set[1])
        valid_set = (valid_data, valid_set[1])
        test_set = (test_data, test_set[1])

    test_set_x, test_set_y = test_set
    valid_set_x, valid_set_y = valid_set
    train_set_x, train_set_y = train_set

    rval = [(train_set_x, train_set_y), (valid_set_x, valid_set_y),
            (test_set_x, test_set_y)]
    return rval


def load_tfd(path):
    train_data = []
    train_labels = []
    with open(path, 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            if len(row) == 2:
                train_labels.append(int(row[0]))
                pixels = [int(p) for p in row[1].split()]
            else:
                pixels = [int(p) for p in row[0].split()]
            train_data.append(pixels)
    train_data = np.asarray(train_data, dtype=theano.config.floatX)
    if train_labels:
        train_labels = np.asarray(train_labels, dtype=theano.config.floatX)
    else:
        train_labels = None
    return (train_data, train_labels)


def extract_patches(images, image_size=(48, 48), patch_size=(8, 8), stride=4):
    """Extracts small patches from images.

    :param images: images from which patches will be extracted
    :type images: list of numpy arrays
    :param image_size: height and witdth of single image
    :type image_size: tuple
    :param patch_size: dimensions of a patch
    :type patch_size: tuple
    :param stride: gap in pixels between locations consecutive patches
    :type stride: int32
    :rtype: numpy ndarray

    """
    patches = []
    for image in images:
        # images are expected be 1D vectors, it's necessary to reshape them
        image_2d = image.reshape(image_size)
        for x in xrange(0, image_size[0] - stride, stride):
            for y in xrange(0, image_size[1] - stride, stride):
                # extract 2D patch
                patch = image_2d[x:x + patch_size[0], y:y + patch_size[1]]
                # flatten it back to 1D
                patches.append(patch.flatten())
    return np.asarray(patches, dtype=theano.config.floatX)


def group_by_class(data, classes):
    """Groups given dataset by classes.

    :param data: data that will be grouped
    :type data: list of numpy arrays
    :param classes: classes[i] is a class for ith example in data
    :type classes: list of ints
    :rtype: dictionary of lists of numpy arrays

    """
    grouped_data = {}
    for i, d in enumerate(data):
#        if classes[i] not in grouped_data:
#            grouped_data[classes[i]] = []
#        grouped_data[classes[i]].append(d)
        grouped_data.setdefault(classes[i], []).append(d)
    return grouped_data
