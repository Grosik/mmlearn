# -*- coding: utf-8 -*-
'''
@author: Michał Grosicki
'''

import numpy as np
import theano
import theano.tensor as T

from models.rbm import RBM


class GaussianRBM(RBM):
    '''
    classdocs
    '''
    def __init__(self, n_visible, n_hidden, input=None, W=None, hbias=None,
                 vbias=None, sigma2=None, numpy_rng=None, theano_rng=None):
        '''
        Constructor
        '''
        if sigma2 is None:
            sigma2 = theano.shared(value=np.ones(n_visible,
                                                dtype=theano.config.floatX),
                                  name='sigma2', borrow=True)
        self.sigma2 = sigma2
        super(GaussianRBM, self).__init__(n_visible, n_hidden, input, W, hbias,
                 vbias, numpy_rng, theano_rng)
        self.params.append(self.sigma2)
        self.old_gparams.append(theano.shared(value=np.zeros_like(
                                        self.sigma2.get_value(borrow=True)),
                                        name='old_sigma2', borrow=True))

    def free_energy(self, v_sample):
        wx_b = T.dot(v_sample / self.sigma2, self.W) + self.hbias
        vbias_term = T.dot((v_sample - self.vbias) / T.sqrt(self.sigma2),
                           ((v_sample - self.vbias) / T.sqrt(self.sigma2)).T) / 2
        hidden_term = T.sum(T.nnet.softplus(wx_b), axis=1)
        return -hidden_term - vbias_term

    def propup(self, vis):
        pre_sigmoid_activation = T.dot(vis / self.sigma2, self.W) + self.hbias
        return [pre_sigmoid_activation, T.nnet.sigmoid(pre_sigmoid_activation)]
#        pre_max_activation = T.dot(vis, self.W) + self.hbias
#        return [pre_max_activation, T.maximum(0, pre_max_activation)]

    def sample_h_given_v(self, v0_sample):
        pre_sigmoid_h1, h1_mean = self.propup(v0_sample)
        h1_sample = self.theano_rng.binomial(size=h1_mean.shape,
                                             n=1, p=h1_mean,
                                             dtype=theano.config.floatX)
#        uniform_sample = self.theano_rng.uniform(size=h1_mean.shape,
#                                                 dtype=theano.config.floatX)
#        h1_sample = T.cast(T.gt(uniform_sample, 0.5),
#                           dtype=theano.config.floatX)
        return [pre_sigmoid_h1, h1_mean, h1_sample]
#        pre_max_h1, h1_mean = self.propup(v0_sample)
#        pre_sample = pre_max_h1 + self.theano_rng.normal(
#                            size=pre_max_h1.shape, avg=0.0,
#                            std=T.sqrt(T.nnet.sigmoid(pre_max_h1)),
#                            dtype=theano.config.floatX)
#        h1_sample = T.maximum(0, pre_sample)
#        return [pre_max_h1, h1_mean, h1_sample]

    def propdown(self, hid):
        pre_sigmoid_activation = T.dot(hid, self.W.T) + self.vbias
#        activation = self.theano_rng.normal(size=pre_sigmoid_activation.shape,
#                                            avg=pre_sigmoid_activation,
#                                            std=self.sigma2,
#                                            dtype=theano.config.floatX)
        return [pre_sigmoid_activation, pre_sigmoid_activation]

    def sample_v_given_h(self, h0_sample):
        pre_sigmoid_v1, v1_mean = self.propdown(h0_sample)
        v1_sample = self.theano_rng.normal(size=pre_sigmoid_v1.shape,
                                           avg=pre_sigmoid_v1,
                                           std=self.sigma2,
                                           dtype=theano.config.floatX)
#        v1_sample = v1_mean
        return [pre_sigmoid_v1, v1_mean, v1_sample]

    def __getstate__(self):
        super_state = super(GaussianRBM, self).__getstate__()
        sigma2 = self.sigma2.get_value(borrow=True)
        return (super_state, sigma2)

    def __setstate__(self, state):
        super_state, sigma2 = state
        W, hbias, vbias = super_state
        W_shared = theano.shared(W, name='W', borrow=True)
        hbias_shared = theano.shared(hbias, name='hbias', borrow=True)
        vbias_shared = theano.shared(vbias, name='vbias', borrow=True)
        sigma2_shared = theano.shared(sigma2, name='sigma2', borrow=True)
        self.__init__(W.shape[0], W.shape[1], W=W_shared, hbias=hbias_shared,
                      vbias=vbias_shared, sigma2=sigma2_shared)
