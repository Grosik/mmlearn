# -*- coding: utf-8 -*-
'''
Copyright (c) 2008–2013, Theano Development Team All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
Neither the name of Theano nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ‘’AS IS’’ AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author: Michał Grosicki
'''
import time

import numpy as np
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from models.rbm import RBM
from models.gaussian_rbm import GaussianRBM


# TODO RBM types
class DBN(object):
    def __init__(self, n_visible, hidden_layers_sizes, numpy_rng=None, params=None,
                 theano_rng=None):
        self.hidden_layers = []
        self.n_layers = len(hidden_layers_sizes)
        assert self.n_layers > 0
        if not numpy_rng:
            numpy_rng = np.random.RandomState(time.gmtime())
        if not theano_rng:
            theano_rng = RandomStreams(numpy_rng.randint(2 ** 30))
        # allocate symbolic variables for the data
        self.x = T.matrix('x')

        for i in xrange(self.n_layers):
            if i == 0:
                input_size = n_visible
            else:
                input_size = hidden_layers_sizes[i - 1]
            if i == 0:
                layer_input = self.x
            else:
                layer_input = self.hidden_layers[-1].output
            if i == 0:
                if params is None:
                    rbm_layer = GaussianRBM(numpy_rng=numpy_rng,
                                        theano_rng=theano_rng,
                                        input=layer_input,
                                        n_visible=input_size,
                                        n_hidden=hidden_layers_sizes[i])
                else:
                    W = params[i][0]
                    hbias = params[i][1]
                    vbias = params[i][2]
                    sigma2 = params[i][3]
                    rbm_layer = GaussianRBM(numpy_rng=numpy_rng,
                                        theano_rng=theano_rng,
                                        input=layer_input,
                                        n_visible=input_size, vbias=vbias,
                                        W=W, hbias=hbias, sigma2=sigma2,
                                        n_hidden=hidden_layers_sizes[i])
            else:
                if params is None:
                    rbm_layer = RBM(numpy_rng=numpy_rng, theano_rng=theano_rng,
                                input=layer_input, n_visible=input_size,
                                n_hidden=hidden_layers_sizes[i])
                else:
                    W = params[i][0]
                    hbias = params[i][1]
                    vbias = params[i][2]
                    rbm_layer = RBM(numpy_rng=numpy_rng, theano_rng=theano_rng,
                                input=layer_input, n_visible=input_size,
                                n_hidden=hidden_layers_sizes[i],
                                W=W, hbias=hbias, vbias=vbias)
            self.hidden_layers.append(rbm_layer)

    def train(self, learning_rates, l1_regs, l2_regs, n_epochs, dataset,
              batch_sizes, n_chains, persistent=True, ks=[1], momentums=[0.9]):
        # training DBN using greedy layer-wise algorithm
        data = dataset
        for i in xrange(len(self.hidden_layers)):
            rbm_layer = self.hidden_layers[i]
            # assume that default value for each hyperparameter is
            # the last element in array
            lr = learning_rates[-1]
            if i < len(learning_rates):
                lr = learning_rates[i]
            l1_reg = l1_regs[-1]
            if i < len(l1_regs):
                l1_reg = l1_regs[i]
            l2_reg = l2_regs[-1]
            if i < len(l2_regs):
                l2_reg = l2_regs[i]
            batch_size = batch_sizes[-1]
            if i < len(batch_sizes):
                batch_size = batch_sizes[i]
            k = ks[-1]
            if i < len(ks):
                k = ks[i]
            m = momentums[-1]
            if i < len(momentums):
                m = momentums[i]
            rbm_layer.train(learning_rate=lr, l1_reg=l1_reg, l2_reg=l2_reg,
                            n_epochs=n_epochs, dataset=data, n_chains=n_chains,
                            batch_size=batch_size, persistent=persistent, k=k,
                            momentum=m)
            # create theano function for computing hidden representation
            data_fn = theano.function(inputs=[rbm_layer.input],
                                      outputs=rbm_layer.output,
                                      allow_input_downcast=True)
            # compute hidden representation
            data = data_fn(data)

    def sample(self, plot_every, n_chains, n_samples, chain_init_state=None):
        # sample from the top RBM
        top_layer = self.hidden_layers[-1]
        # if there is initial state for the Markov chain propagate it up
        # through DBN
        # TODO
        if chain_init_state:
            pass
        print "Sampling from top-level RBM..."
        samples = top_layer.sample(plot_every=plot_every, n_samples=n_samples,
                         n_chains=n_chains, chain_init_state=chain_init_state)
        s = T.matrix("s")
        # propagate samples down through the network
        print "Propagating samples down through layers..."
        for i in xrange(self.n_layers - 2, -1, -1):
            print "..layer " + str(i)
            layer = self.hidden_layers[i]
            new_samples = np.zeros(shape=(n_samples, n_chains,
                                          layer.n_visible))
            propdown_fn = theano.function(allow_input_downcast=True,
                                          inputs=[s],
                                          outputs=layer.propdown(s)[1])
            for j in xrange(n_samples):
                new_samples[j] = propdown_fn(samples[j])
            samples = new_samples
        return samples

    def propup(self, data):
        for rbm_layer in self.hidden_layers:
            data_fn = theano.function(inputs=[rbm_layer.input],
                                      outputs=rbm_layer.output,
                                      allow_input_downcast=True)
            data = data_fn(data)
        return data

    def params(self):
        p = []
        for layer in self.hidden_layers:
            p.append(layer.params)
        return p

    def __getstate__(self):
        params = []
        for i in xrange(self.n_layers):
            if i == 0:
                params.append((self.hidden_layers[i].W.get_value(borrow=True),
                   self.hidden_layers[i].hbias.get_value(borrow=True),
                   self.hidden_layers[i].vbias.get_value(borrow=True),
                   self.hidden_layers[i].sigma2.get_value(borrow=True)))
            else:
                params.append((self.hidden_layers[i].W.get_value(borrow=True),
                   self.hidden_layers[i].hbias.get_value(borrow=True),
                   self.hidden_layers[i].vbias.get_value(borrow=True)))
        return params

    def __setstate__(self, state):
        params = []
        hidden_layers_sizes = []
        for i in xrange(len(state)):
            W = state[i][0]
            hbias = state[i][1]
            vbias = state[i][2]
            if len(state[i]) == 4:
                sigma2 = state[i][3]
            if i == 0:
                n_visible = W.shape[0]
            if i < len(state):
                hidden_layers_sizes.append(W.shape[1])
            W_shared = theano.shared(W, name='W', borrow=True)
            hbias_shared = theano.shared(hbias, name='hbias', borrow=True)
            vbias_shared = theano.shared(vbias, name='vbias', borrow=True)
            p = (W_shared, hbias_shared, vbias_shared)
            if len(state[i]) == 4:
                sigma2_shared = theano.shared(sigma2, name='sigma2',
                                              borrow=True)
                p = p + (sigma2_shared,)
            params.append(p)
        self.__init__(n_visible, hidden_layers_sizes, params=params)
