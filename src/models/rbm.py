# -*- coding: utf-8 -*-
'''
Copyright (c) 2008–2013, Theano Development Team All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
Neither the name of Theano nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ‘’AS IS’’ AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author: Michał Grosicki
'''
import time
import cPickle

import numpy as np
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

#from utils import tile_raster_images


class RBM(object):
    def __init__(self, n_visible, n_hidden, input=None, W=None, hbias=None,
                 vbias=None, numpy_rng=None, theano_rng=None):
        # Initialize RBM as input or hidden layer
        self.input = input if input else T.matrix('x')
        self.n_visible = n_visible
        self.n_hidden = n_hidden

        if numpy_rng is None:
            # create a number generator
            numpy_rng = np.random.RandomState(1234)

        if theano_rng is None:
            theano_rng = RandomStreams(numpy_rng.randint(2 ** 30))

        if W is None:
            # W is initialized with `initial_W` which is uniformely
            # sampled from -4*sqrt(6./(n_visible+n_hidden)) and
            # 4*sqrt(6./(n_hidden+n_visible)) the output of uniform if
            # converted using asarray to dtype theano.config.floatX so
            # that the code is runable on GPU
            initial_W = np.asarray(numpy_rng.uniform(
                      low=-4 * np.sqrt(6. / (n_hidden + n_visible)),
                      high=4 * np.sqrt(6. / (n_hidden + n_visible)),
                      size=(n_visible, n_hidden)),
                      dtype=theano.config.floatX)
            # theano shared variables for weights and biases
            W = theano.shared(value=initial_W, name='W', borrow=True)

        if hbias is None:
            # create shared variable for hidden units bias
            hbias = theano.shared(value=np.zeros(n_hidden,
                                                dtype=theano.config.floatX),
                                  name='hbias', borrow=True)

        if vbias is None:
            # create shared variable for visible units bias
            vbias = theano.shared(value=np.zeros(n_visible,
                                                 dtype=theano.config.floatX),
                                  name='vbias', borrow=True)

        self.W = W
        self.hbias = hbias
        self.vbias = vbias
        self.theano_rng = theano_rng
        self.output = self.propup(self.input)[1]
        self.params = [self.W, self.hbias, self.vbias]
        self.old_gparams = [theano.shared(value=np.zeros_like(self.W.get_value(borrow=True)), name='old_W', borrow=True),
                            theano.shared(value=np.zeros_like(self.hbias.get_value(borrow=True)), name='old_hbias', borrow=True),
                            theano.shared(value=np.zeros_like(self.vbias.get_value(borrow=True)), name='old_vbias', borrow=True)]

    def free_energy(self, v_sample):
        wx_b = T.dot(v_sample, self.W) + self.hbias
        vbias_term = T.dot(v_sample, self.vbias)
        hidden_term = T.sum(T.log(1 + T.exp(wx_b)), axis=1)
        return -hidden_term - vbias_term

    def propup(self, vis):
        pre_sigmoid_activation = T.dot(vis, self.W) + self.hbias
        return [pre_sigmoid_activation, T.nnet.sigmoid(pre_sigmoid_activation)]

    def sample_h_given_v(self, v0_sample):
        pre_sigmoid_h1, h1_mean = self.propup(v0_sample)
        h1_sample = self.theano_rng.binomial(size=h1_mean.shape,
                                             n=1, p=h1_mean,
                                             dtype=theano.config.floatX)
        return [pre_sigmoid_h1, h1_mean, h1_sample]

    def propdown(self, hid):
        pre_sigmoid_activation = T.dot(hid, self.W.T) + self.vbias
        return [pre_sigmoid_activation, T.nnet.sigmoid(pre_sigmoid_activation)]

    def sample_v_given_h(self, h0_sample):
        pre_sigmoid_v1, v1_mean = self.propdown(h0_sample)
        v1_sample = self.theano_rng.binomial(size=v1_mean.shape,
                                             n=1, p=v1_mean,
                                             dtype=theano.config.floatX)
        return [pre_sigmoid_v1, v1_mean, v1_sample]

    def gibbs_hvh(self, h0_sample):
        pre_sigmoid_v1, v1_mean, v1_sample = self.sample_v_given_h(h0_sample)
        pre_sigmoid_h1, h1_mean, h1_sample = self.sample_h_given_v(v1_sample)
        return [pre_sigmoid_v1, v1_mean, v1_sample,
                pre_sigmoid_h1, h1_mean, h1_sample]

    def gibbs_vhv(self, v0_sample):
        pre_sigmoid_h1, h1_mean, h1_sample = self.sample_h_given_v(v0_sample)
        pre_sigmoid_v1, v1_mean, v1_sample = self.sample_v_given_h(h1_sample)
        return [pre_sigmoid_h1, h1_mean, h1_sample,
                pre_sigmoid_v1, v1_mean, v1_sample]

    def get_cost_updates(self, lr=0.1, l1_reg=0.0, l2_reg=0.0, persistent=None,
                         k=1, momentum=0.9):
        # compute positive phase
        pre_sigmoid_ph, ph_mean, ph_sample = self.sample_h_given_v(self.input)

        # decide how to initialize persistent chain:
        # for CD, we use the newly generate hidden sample
        # for PCD, we initialize from the old state of the chain
        if persistent is None:
            chain_start = ph_sample
        else:
            chain_start = persistent

        # perform actual negative phase, use scan for this
        [pre_sigmoid_nvs, nv_means, nv_samples,
         pre_sigmoid_nhs, nh_means, nh_samples], updates = \
            theano.scan(self.gibbs_hvh,
                    # the None are place holders, saying that
                    # chain_start is the initial state corresponding to the
                    # 6th output
                    outputs_info=[None,  None,  None, None, None, chain_start],
                    n_steps=k)
        # determine gradients on RBM parameters
        # not that we only need the sample at the end of the chain
        chain_end = nv_samples[-1]

        cost = T.mean(self.free_energy(self.input)) - \
            T.mean(self.free_energy(chain_end))
        cost += l1_reg * abs(self.params[0]).sum()
        cost += l2_reg * T.sqrt((self.params[0] ** 2).sum())
        # We must not compute the gradient through the gibbs sampling
        gparams = T.grad(cost, self.params, consider_constant=[chain_end])

        # constructs the update dictionary
#        try:
#            iter(momentum)
#        except Exception:
#            momentum = [momentum for p in self.params]
        momentum_value = momentum
        momentum = []
        for p in self.params:
            m = np.zeros_like(p.get_value(borrow=True),
                              dtype=theano.config.floatX)
            m.fill(momentum_value)
            momentum.append(theano.shared(m))
        momentum_step = (0.9 - momentum_value) / (20 * 20)
        for p, gp, ogp, m in zip(self.params, gparams, self.old_gparams, momentum):
            updates[p] = p + lr * (m * ogp - (1.0 - m) * gp)
            updates[m] = T.minimum(m + momentum_step, 0.9)
        self.old_gparams = gparams
#        for gparam, param in zip(gparams, self.params):
#            # make sure that the learning rate is of the right dtype
#            updates[param] = param - gparam * T.cast(lr,
#                                                    dtype=theano.config.floatX)

        if persistent:
            # Note that this works only if persistent is a shared variable
            updates[persistent] = nh_samples[-1]
            # pseudo-likelihood is a better proxy for PCD
            monitoring_cost = self.get_pseudo_likelihood_cost(updates)
        else:
            # reconstruction cross-entropy is a better proxy for CD
            monitoring_cost = self.get_reconstruction_cost(updates,
                                                           pre_sigmoid_nvs[-1])

        return monitoring_cost, updates

    def get_pseudo_likelihood_cost(self, updates):
        """Stochastic approximation to the pseudo-likelihood"""

        # index of bit i in expression p(x_i | x_{\i})
        bit_i_idx = theano.shared(value=0, name='bit_i_idx')

        # binarize the input image by rounding to nearest integer
        xi = T.round(self.input)

        # calculate free energy for the given bit configuration
        fe_xi = self.free_energy(xi)

        # flip bit x_i of matrix xi and preserve all other bits x_{\i}
        # Equivalent to xi[:,bit_i_idx] = 1-xi[:, bit_i_idx], but assigns
        # the result to xi_flip, instead of working in place on xi.
        xi_flip = T.set_subtensor(xi[:, bit_i_idx], 1 - xi[:, bit_i_idx])

        # calculate free energy with bit flipped
        fe_xi_flip = self.free_energy(xi_flip)

        # equivalent to e^(-FE(x_i)) / (e^(-FE(x_i)) + e^(-FE(x_{\i})))
        cost = T.mean(self.n_visible * T.log(T.nnet.sigmoid(fe_xi_flip -
                                                            fe_xi)))

        # increment bit_i_idx % number as part of updates
        updates[bit_i_idx] = (bit_i_idx + 1) % self.n_visible

        return cost

    def get_reconstruction_cost(self, updates, pre_sigmoid_nv):
        """Approximation to the reconstruction error

        """
        cross_entropy = T.mean(
                T.sum(self.input * T.log(T.nnet.sigmoid(pre_sigmoid_nv)) +
                (1 - self.input) * T.log(1 - T.nnet.sigmoid(pre_sigmoid_nv)),
                      axis=1))

        return cross_entropy

    def train(self, learning_rate, l1_reg, l2_reg, n_epochs, dataset,
              batch_size, n_chains, persistent=True, k=1, momentum=0.5):
        n_train_batches = dataset.shape[0] / batch_size
        # allocate symbolic variables for the data
        index = T.lscalar()    # index to a [mini]batch
        # initialize storage for the persistent chain (state = hidden
        # layer of chain)
        dataset = theano.shared(np.asarray(dataset, dtype=theano.config.floatX),
                                borrow=True)
        persistent_chain = None
        if persistent:
            persistent_chain = theano.shared(np.zeros((batch_size,
                                                       self.n_hidden),
                                                      dtype=theano.config.floatX),
                                             borrow=True)
        cost, updates = self.get_cost_updates(lr=learning_rate, k=k,
                                              l1_reg=l1_reg, l2_reg=l2_reg,
                                              momentum=momentum,
                                              persistent=persistent_chain)

        # it is ok for a theano function to have no output
        # the purpose of train_rbm is solely to update the RBM parameters
        train_rbm = theano.function([index], cost,
               updates=updates,
               givens={self.input: dataset[index * batch_size:
                                      (index + 1) * batch_size]},
               name='train_rbm')
        plotting_time = 0.
        start_time = time.clock()
        # go through training epochs
        for epoch in xrange(n_epochs):
            # go through the training set
            mean_cost = []
            for batch_index in xrange(n_train_batches):
                mean_cost += [train_rbm(batch_index)]

            print 'Training epoch %d, cost is ' % epoch, np.mean(mean_cost)
        end_time = time.clock()

        pretraining_time = (end_time - start_time) - plotting_time

        print ('Training took %f minutes' % (pretraining_time / 60.))

    def sample(self, plot_every, n_chains, n_samples, chain_init_state=None):
        if chain_init_state is None:
            chain_start = np.asarray(np.random.rand(n_chains, self.n_visible),
                                     dtype=theano.config.floatX)
        else:
            numpy_rng = np.random.RandomState(1234)
            number_of_init_samples = chain_init_state.shape[0]
            test_idx = numpy_rng.randint(number_of_init_samples - n_chains)
            chain_start = np.asarray(
                chain_init_state[test_idx:test_idx + n_chains],
                dtype=theano.config.floatX)
        persistent_vis_chain = theano.shared(chain_start)
        [presig_hids, hid_mfs, hid_samples, presig_vis,
         vis_mfs, vis_samples], updates = \
                        theano.scan(self.gibbs_vhv,
                                outputs_info=[None,  None, None, None,
                                              None, persistent_vis_chain],
                                n_steps=plot_every)
        updates.update({persistent_vis_chain: vis_samples[-1]})
        sample_fn = theano.function([], [vis_mfs[-1], vis_samples[-1]],
                                    updates=updates, name='sample_fn')
        samples = np.zeros(shape=(n_samples, n_chains, self.n_visible))
        for idx in xrange(n_samples):
            vis_mf, vis_sample = sample_fn()
            samples[idx] = vis_mf
        return samples

    def __getstate__(self):
        W = self.W.get_value(borrow=True)
        hbias = self.hbias.get_value(borrow=True)
        vbias = self.vbias.get_value(borrow=True)
        return (W, hbias, vbias)

    def __setstate__(self, state):
        W, hbias, vbias = state
        W_shared = theano.shared(W, name='W', borrow=True)
        hbias_shared = theano.shared(hbias, name='hbias', borrow=True)
        vbias_shared = theano.shared(vbias, name='vbias', borrow=True)
        self.__init__(W.shape[0], W.shape[1], W=W_shared, hbias=hbias_shared,
                      vbias=vbias_shared)
