# -*- coding: utf-8 -*-
'''
Copyright (c) 2008–2013, Theano Development Team All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
Neither the name of Theano nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ‘’AS IS’’ AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author: Michał Grosicki
'''
import time

import numpy as np
import theano
import theano.tensor as T


class LinearLayer(object):
    def __init__(self, input, n_in, n_out, W=None, b=None, rng=None):
        self.input = input
        if rng is None:
            rng = np.random.RandomState(time.gmtime())
        if W is None:
            W_vals = np.asarray(rng.uniform(low=-np.sqrt(6. / (n_in + n_out)),
                                            high=np.sqrt(6. / (n_in + n_out)),
                                        size=(n_in, n_out)),
                                dtype=theano.config.floatX)
            W = theano.shared(value=W_vals, name='W', borrow=True)

        if b is None:
            b_values = np.zeros((n_out), dtype=theano.config.floatX)
            b = theano.shared(value=b_values, name='b', borrow=True)
        self.W = W
        self.b = b
        self.output = T.dot(self.input, self.W) + self.b
        self.params = [self.W, self.b]


class SigmoidLayer(LinearLayer):
    def __init__(self, input, n_in, n_out, W=None, b=None, rng=None):
        if W is None:
            W_vals = np.asarray(rng.uniform(low=-np.sqrt(6. / (n_in + n_out)),
                                            high=np.sqrt(6. / (n_in + n_out)),
                                            size=(n_in, n_out)),
                                dtype=theano.config.floatX) * 4
            W = theano.shared(value=W_vals, name='W', borrow=True)
        super(SigmoidLayer, self).__init__(input, n_in, n_out, W=W, b=b, rng=rng)
        self.output = T.nnet.sigmoid(self.output)


class TanhLayer(LinearLayer):
    def __init__(self, input, n_in, n_out, W=None, b=None, rng=None):
        super(TanhLayer, self).__init__(input, n_in, n_out, W, b, rng=rng)
        self.output = T.tanh(self.output)


# TODO figure out how to add n_pieces to the constructor
# handle the case when layers have different numbers of outputs than units
class MaxoutLayer(LinearLayer):
    def __init__(self, input, n_in, n_out, W=None, b=None, rng=None):
        n_pieces = 5
        super(MaxoutLayer, self).__init__(input, n_in, n_out, W, b, rng=rng)
        last_start = n_out - n_pieces
        p = None
        for i in xrange(n_pieces):
            cur = self.output[:, i:last_start + i + 1:n_pieces]
            if p is None:
                p = cur
            else:
                p = T.maximum(cur, p)
        self.output = p
