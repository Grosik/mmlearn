# -*- coding: utf-8 -*-
'''
Copyright (c) 2008–2013, Theano Development Team All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
Neither the name of Theano nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ‘’AS IS’’ AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author: Michał Grosicki
'''

import os
import sys
import time

import numpy as np
import theano
import theano.tensor as T

from layer import SigmoidLayer
from softmax_layer import SoftmaxLayer
from util.datasets import shared_dataset

'''
TODO
Implement other types of layers
'''


class MLP(object):
    def __init__(self, n_visible, hidden_layers_sizes, n_out,
                 hidden_layers_type=SigmoidLayer, params=None, rng=None):
        self.n_layers = len(hidden_layers_sizes)
        # create symbolic variables for theano functions
        self.x = T.matrix('x')
        self.y = T.ivector('y')
        # create hidden layers
        self.hidden_layers = []
        # create first hidden layer connected to input layer
        W = None
        b = None
        if rng is None:
            rng = np.random.RandomState(time.gmtime())
        if params:
            W = params[0][0]
            b = params[0][1]
        if hidden_layers_sizes:
            self.hidden_layers.append(hidden_layers_type(rng=rng, input=self.x,
                                                  n_out=hidden_layers_sizes[0],
                                                  W=W, b=b, n_in=n_visible))
        # create the rest of the layers
        for i in xrange(1, self.n_layers):
            if params and i < len(params):
                W = params[i][0]
                b = params[i][1]
            else:
                W = None
                b = None
            self.hidden_layers.append(hidden_layers_type(rng=rng, W=W, b=b,
                                                  input=self.hidden_layers[i - 1].output,
                                                  n_in=hidden_layers_sizes[i - 1],
                                                  n_out=hidden_layers_sizes[i]))
        # create output layer
        if self.hidden_layers:
            self.softmax_layer = SoftmaxLayer(input=self.hidden_layers[-1].output,
                                              n_in=hidden_layers_sizes[-1],
                                              n_out=n_out)
        else:
            self.softmax_layer = SoftmaxLayer(input=self.x, n_in=n_visible,
                                              n_out=n_out)
        # compute L1 and L2 norms
        self.l1 = 0
        self.l2_sqr = 0
        for i in xrange(self.n_layers):
            self.l1 += abs(self.hidden_layers[i].W).sum()
            self.l2_sqr += (self.hidden_layers[i].W ** 2).sum()
        self.l1 += abs(self.softmax_layer.W).sum()
        self.l2_sqr += (self.softmax_layer.W ** 2).sum()
        self.neg_log_likelihood = self.softmax_layer.negative_log_likelihood
        self.errors = self.softmax_layer.errors
        self.y_pred = self.softmax_layer.y_pred
        self.params = []
        for i in xrange(self.n_layers):
            self.params += self.hidden_layers[i].params
        self.params += self.softmax_layer.params

# TODO handle various types of layers
    def train(self, learning_rate, l1_reg, l2_reg, n_epochs, datasets,
              batch_size, patience, momentum=0.9):
#        try:
#            iter(momentum)
#        except Exception:
        momentum_value = momentum
        momentum = []
        for p in self.params:
            m = np.zeros_like(p.get_value(borrow=True),
                              dtype=theano.config.floatX)
            m.fill(momentum_value)
            momentum.append(theano.shared(m))
        # compute number of minibatches for training, validation and testing
        n_train_batches = (datasets[0][0].shape[0] + batch_size // 2) // batch_size
        n_valid_batches = datasets[1][0].shape[0] / batch_size  # (datasets[1][0].shape[0] + batch_size // 2) // batch_size
        n_test_batches = (datasets[2][0].shape[0] + batch_size // 2) // batch_size
        train_set_x, train_set_y = shared_dataset(datasets[0])
        valid_set_x, valid_set_y = shared_dataset(datasets[1])
        test_set_x, test_set_y = shared_dataset(datasets[2])
        # allocate symbolic variables for the data
        index = T.lscalar()  # index to a [mini]batch

        cost = self.neg_log_likelihood(self.y) + l1_reg * self.l1 + \
            l2_reg * self.l2_sqr
        # compiling a Theano function that computes the mistakes that are made
        # by the model on a minibatch
        if test_set_y is None:
            test_model = theano.function(inputs=[index],
                    outputs=self.y_pred,
                    givens={
                        self.x: test_set_x[index * batch_size:(index + 1) *
                                batch_size]})
        else:
            test_model = theano.function(inputs=[index],
                    outputs=self.errors(self.y),
                    givens={
                        self.x: test_set_x[index * batch_size:(index + 1) *
                                batch_size],
                        self.y: test_set_y[index * batch_size:(index + 1) *
                                batch_size]})

        validate_model = theano.function(inputs=[index],
                outputs=self.errors(self.y),
                givens={
                    self.x: valid_set_x[index * batch_size:(index + 1) *
                                        batch_size],
                    self.y: valid_set_y[index * batch_size:(index + 1) *
                                        batch_size]})

        # compute the gradient of cost with respect to theta (sotred in params)
        # the resulting gradients will be stored in a list gparams
        gparams = []
        for param in self.params:
            gparam = T.grad(cost, param)
            gparams.append(gparam)
        # specify how to update the parameters of the model as a list of
        # (variable, update expression) pairs
        changes = [theano.shared(np.zeros_like(p.get_value(borrow=True),
                                               dtype=theano.config.floatX))
                   for p in self.params]
        # calculate how much will momentum increase after one interation
        # (not epoch!)
        momentum_step = (0.9 - momentum_value) / (n_train_batches * 20)
        updates = []
        for p, gp, c, m in zip(self.params, gparams, changes, momentum):
#        for p, gp, c in zip(self.params, gparams, changes):
            updates.append((p, p + learning_rate * c))
            updates.append((c, m * c - (1.0 - m) * gp))
            updates.append((m, T.minimum(m + momentum_step, 0.9)))

        # compiling a Theano function `train_model` that returns the cost, but
        # in the same time updates the parameter of the model based on the
        # rules defined in `updates`
        train_model = theano.function(inputs=[index], outputs=cost,
                updates=updates,
                givens={
                    self.x: train_set_x[index * batch_size:(index + 1) *
                                        batch_size],
                    self.y: train_set_y[index * batch_size:(index + 1) *
                                        batch_size]})
        # early-stopping parameters
        # patience - look as this many examples regardless
        patience_increase = 2   # wait this much longer when a new best is found
        improvement_threshold = 0.995   # a relative improvement of this much is considered significant
        validation_frequency = min(n_train_batches, patience / 2)
        # go through this many
        # minibatche before checking the network
        # on the validation set; in this case we
        # check every epoch

    #    best_params = None
        best_loss = np.inf
        best_iter = 0
        test_score = 0.
        start_time = time.clock()

        epoch = 0
        done_looping = False

        while (epoch < n_epochs) and (not done_looping):
            epoch = epoch + 1
            for minibatch_index in xrange(n_train_batches):
                minibatch_avg_cost = train_model(minibatch_index)
                # iteration number
                iteration = epoch * n_train_batches + minibatch_index
                if (iteration + 1) % validation_frequency == 0:
                    # compute zero-one loss on validation set
                    validation_losses = [validate_model(i) for i
                                   in xrange(n_valid_batches)]
                    current_loss = np.mean(validation_losses)
                    print('epoch %i, minibatch %i/%i, validation error %f %%, momentum: %f' %
                         (epoch, minibatch_index + 1, n_train_batches,
                          current_loss * 100., momentum[0].get_value(borrow=True)[0][0]))

                    # if we got the best validation score until now
                    if current_loss < best_loss:
                        #improve patience if loss improvement is good enough
                        if current_loss < best_loss * improvement_threshold:
                            patience = max(patience,
                                           iteration * patience_increase)

                            best_loss = current_loss
                            best_iter = iteration
                            best_params = self.params

                            # test it on the test set
                            if test_set_y is None:
                                predictions = [test_model(i) for i
                                     in xrange(n_test_batches)]
                                predictions = np.concatenate(predictions)
                                np.savetxt("/home/grosik/predictions.csv",
                                           predictions, delimiter=",", fmt="%d")
                            else:
                                test_losses = [test_model(i) for i
                                     in xrange(n_test_batches)]
                                test_score = np.mean(test_losses)

                                print(('     epoch %i, minibatch %i/%i, test error of '  'best model %f %%') % (epoch, minibatch_index + 1, n_train_batches, test_score * 100.))

                if patience <= iteration:
                    done_looping = True
                    break

            end_time = time.clock()
        self.params = best_params
        print(('Optimization complete. Best validation score of %f %% '
               'obtained at iteration %i, with test performance %f %%') %
              (best_loss * 100., best_iter, test_score * 100.))
        print >> sys.stderr, ('The code for file ' + os.path.split(__file__)[1]
                              + ' ran for %.2fm' % ((end_time - start_time) / 60.))

    def __getstate__(self):
        params = [(self.hidden_layers[i].W.get_value(borrow=True),
                   self.hidden_layers[i].b.get_value(borrow=True))
                  for i in xrange(self.n_layers)]
        params.append((self.softmax_layer.W.get_value(borrow=True),
                       self.softmax_layer.b.get_value(borrow=True)))
        return params

    def __setstate__(self, state):
        params = []
        hidden_layers_sizes = []
        for i in xrange(len(state)):
            W = state[i][0]
            b = state[i][1]
            if i == 0:
                n_visible = W.shape[0]
            if i < len(state) - 1:
                hidden_layers_sizes.append(W.shape[1])
            else:
                n_out = W.shape[1]
            W_shared = theano.shared(W, name='W', borrow=True)
            b_shared = theano.shared(b, name='b', borrow=True)
            params.append((W_shared, b_shared))
        self.__init__(n_visible, hidden_layers_sizes, n_out,
                 hidden_layers_type=SigmoidLayer, params=params)
